<?php namespace Xeor\Minify\Console;

use Exception;
use Illuminate\Console\Command;
use Xeor\Minify\Classes\MinifyAssets;

class MinifyClear extends Command
{
    /**
     * The console command name.
     * @var string
     */
    protected $name = 'minify:clear';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Clear all minified files.';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function fire()
    {
        try {
            MinifyAssets::resetCache();
        }
        catch (Exception $ex) {
            $this->error($ex->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
