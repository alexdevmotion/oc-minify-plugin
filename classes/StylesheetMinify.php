<?php namespace Xeor\Minify\Classes;

use Assetic\Asset\AssetInterface;
use Assetic\Filter\FilterInterface;
use Xeor\Minify\Classes\CSSMin;

/**
 * Minify CSS Filter
 * Class used to compress stylesheet css files.
 */
class StylesheetMinify implements FilterInterface
{
    public function filterLoad(AssetInterface $asset) {}

    public function filterDump(AssetInterface $asset)
    {
        $cssmin = new CSSMin(TRUE);
        // Compress the CSS splitting lines after 4k of text.
        $asset->setContent($cssmin->run($asset->getContent(), 4096));
    }
}
