<?php namespace Xeor\Minify\Classes;

use Lang;
use ApplicationException;
use Xeor\Minify\Classes\MinifyAssets;
use Illuminate\Routing\Controller as ControllerBase;
use Exception;

/**
 * The System controller class.
 *
 * @package october\system
 * @author Alexey Bobkov, Samuel Georges
 */
class Controller extends ControllerBase
{
    /**
     * Combines JavaScript and StyleSheet assets.
     * @param string $name Combined file code
     * @return string Combined content.
     */
    public function minify($name)
    {
        try {

            if (!strpos($name, '-')) {
                throw new ApplicationException(Lang::get('system::lang.combiner.not_found', ['name' => $name]));
            }

            $parts = explode('-', $name);
            $cacheId = $parts[0];

            $minifier = MinifyAssets::instance();
            return $minifier->getContents($cacheId);

        }
        catch (Exception $ex) {
            return '/* '.$ex->getMessage().' */';
        }
    }
}
