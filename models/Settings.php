<?php namespace Xeor\Minify\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'minify_settings';

    public $settingsFields = 'fields.yaml';
}