<?php namespace Xeor\Minify\FormWidgets;

use Lang;
use Flash;
use Exception;
use Backend\Classes\FormWidgetBase;
use Xeor\Minify\Classes\MinifyAssets;

class ClearCache extends FormWidgetBase
{
    public function widgetDetails()
    {
        return [
            'name'        => 'xeor.minify::lang.settings.clear_cache',
            'description' => ''
        ];
    }

    public function render(){
        return $this->makePartial('widget');
    }

    public function onClear(){
        try {
            MinifyAssets::resetCache();
            Flash::success(Lang::get('xeor.minify::lang.settings.success'));
        }
        catch (Exception $ex) {
            $this->error($ex->getMessage());
        }
    }
}
