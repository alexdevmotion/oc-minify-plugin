<?php namespace Xeor\Minify;

use URL;
use Event;
use Config;
use BackendAuth;
use Cms\Classes\Theme;
use System\Classes\PluginBase;
use Xeor\Minify\Classes\Minify;
use Xeor\Minify\Models\Settings;
use System\Classes\SettingsManager;
use Xeor\Minify\Classes\MinifyAssets;

/**
 * Minify Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * @var bool Minify asset files.
     */
    public $useMinify;

    /**
     * @var boolean
     */

    public $debug;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'xeor.minify::lang.plugin.name',
            'description' => 'xeor.minify::lang.plugin.description',
            'author'      => 'Sozonov Alexey',
            'icon'        => 'icon-compress'
        ];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsole();
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $settings = Settings::instance();
        $this->debug = isset($settings->is_disabled) && $settings->is_disabled == '1' ? true : false;
        $this->useMinify = isset($settings->is_enabled) && $settings->is_enabled == '1' ? true : false;

        if ($this->useMinify) {
            define('REPLACEMENT_HASH', 'MINIFYHTML' . md5($_SERVER['REQUEST_TIME']));
            Event::listen('cms.page.postprocess', function ($controller, $url, $page, $dataHolder) {
                if (!BackendAuth::check() || (BackendAuth::check() && !$this->debug)) {
                    $dataHolder->content = Minify::page($controller, $dataHolder->content);
                }
            });
        }
    }

    /**
     * Register the settings.
     *
     * @return array
     */
	public function registerSettings()
	{
		$result = [];
		if (\BackendAuth::getUser()->hasAccess('xeor.minify.access_settings')) {
			$result['settings'] = [
				'label'       => 'xeor.minify::lang.settings.menu_label',
				'description' => 'xeor.minify::lang.settings.menu_description',
				'category'    => SettingsManager::CATEGORY_CMS,
				'icon'        => 'icon-compress',
				'class'       => 'Xeor\Minify\Models\Settings',
				'order'       => 500
			];
		}

		return $result;
	}

    /**
     * Register command line specifics
     */
    protected function registerConsole()
    {
        /*
         * Register console commands
         */
        $this->registerConsoleCommand('minify.clear', 'Xeor\Minify\Console\MinifyClear');
    }

    /**
     * Register the markup tags.
     *
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'minify' => [$this, 'minifyFilter']
            ]
        ];
    }

    public function minifyFilter($url = null)
    {
        $themeDir = Theme::getActiveThemeCode();

        if (is_array($url)) {
            $_url = URL::to(MinifyAssets::minify($url, themes_path().'/'.$themeDir));
        }
        else {
            $_url = Config::get('cms.themesPath', '/themes').'/'.$themeDir;
            if ($url !== null) {
                $_url .= '/'.$url;
            }
            $_url = URL::asset($_url);
        }

        return $_url;
    }

    /**
     * Register the form widgets.
     *
     * @return array
     */
    public function registerFormWidgets()
    {
        return [
            'Xeor\Minify\FormWidgets\ClearCache' => [
                'label' => 'xeor.minify::lang.settings.clear_cache',
                'code' => 'clearcache'
            ]
        ];
    }

	/**
	 * Register the permissions.
	 *
	 * @return array
	 */
	public function registerPermissions()
	{
		return [
			'xeor.minify.access_settings' => [
				'label' => 'Access Minify settings',
				'tab' => 'Minify'
			]
		];
	}

}
