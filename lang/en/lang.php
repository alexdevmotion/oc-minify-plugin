<?php

return [
    'plugin' => [
        'name' => 'Minify',
        'description' => 'Provides a mechanism to minify HTML.',
    ],
    'settings' => [
        'menu_label' => 'Minify',
        'menu_description' => 'Provides a mechanism to minify HTML.',
        'settings' => 'Settings',
        'caching' => 'Caching',
        'is_disabled' => 'Disable for backend users',
        'is_enabled' => 'Minify HTML',
        'is_asset_minify_enabled' => 'Minify CSS and JS files',
        'is_yui_enabled' => 'Use YUI Compressor',
        'is_asset_cache_enabled' => 'Cache CSS and JS files',
        'clear_cache' => 'Clear Cache',
        'success' => 'Cache cleared!',
    ]
];