<?php

return [
    'plugin' => [
        'name' => 'Minify',
        'description' => 'Fournit un mécanisme pour minifier le HTML.',
    ],
    'settings' => [
        'menu_label' => 'Minify',
        'menu_description' => 'Fournit un mécanisme pour minifier le HTML.',
        'settings' => 'Paramètres',
        'caching' => 'Mise en cache',
        'is_disabled' => 'Désactiver pour les utilisateurs du backend',
        'is_enabled' => 'Minifier le HTML',
        'is_asset_minify_enabled' => 'Minifier les fichiers CSS et JS',
        'is_yui_enabled' => 'Utiliser YUI Compressor',
        'is_asset_cache_enabled' => 'Mettre en cache les fichiers CSS et JS',
        'clear_cache' => 'Vider le cache',
        'success' => 'Cache effacé !',
    ]
];