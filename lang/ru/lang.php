<?php

return [
    'plugin' => [
        'name' => 'Minify',
        'description' => 'Предоставляет механизм минимизации HTML кода.',
    ],
    'settings' => [
        'menu_label' => 'Minify',
        'menu_description' => 'Предоставляет механизм минимизации HTML кода.',
        'settings' => 'Настройки',
        'caching' => 'Кэширование',
        'is_disabled' => 'Отключить для суперпользователей',
        'is_enabled' => 'Минимизировать HTML',
        'is_asset_minify_enabled' => 'Минимизировать CSS и JS файлы',
        'is_yui_enabled' => 'Использовать YUI Compressor',
        'is_asset_cache_enabled' => 'Кэшировать CSS и JS файлы',
        'clear_cache' => 'Очистить Кэш',
        'success' => 'Кэш очищен!',
    ]
];