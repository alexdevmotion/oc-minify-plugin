<?php

return [
    'plugin' => [
        'name' => 'Minify',
        'description' => 'Παρέχει ένα μηχανισμό για την σμίκρυνση του HTML κώδικα.',
    ],
    'settings' => [
        'menu_label' => 'Minify',
        'menu_description' => 'Παρέχει ένα μηχανισμό για την σμίκρυνση του HTML κώδικα.',
        'settings' => 'Ρυθμίσεις',
        'caching' => 'Κρυφή Μνήμη',
        'is_disabled' => 'Απενεργοποίηση για τους χρήστες του backend',
        'is_enabled' => 'Σμίκρυνση HTML',
        'is_asset_minify_enabled' => 'Σμίκρυνση των αρχείων CSS και JS',
        'is_yui_enabled' => 'Χρήση του YUI Compressor',
        'is_asset_cache_enabled' => 'Χρήση της κρυφής μνήμης (cache) για τα αρχεία CSS και JS',
        'clear_cache' => 'Άδειασμα κρυφής μνήμης',
        'success' => 'Η κρυφή μνήμη καθάρισέ!',
    ]
];