# Minify Plugin for OctoberCMS

This plugin provides the mechanism to render the page using minified version of HTML, JS and CSS files.
Minified HTML is generated using regular expression, JavaScript files are generated using Core Compressor and
CSS files are generated using Core or YUI Compressor.

## How Minify help

Minify removes the comments and whitespace which will help to reduce the file size. 
Smaller HTML and file size reduces the page load time and improve the website performance.


See the [Documentation tab](http://octobercms.com/plugin/xeor-minify#documentation) for the usage details.